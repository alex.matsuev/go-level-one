package main

import (
	"flag"
	"fmt"
	"go-level-one/homework-07/calcWithInterfaces/calc"
	"log"
	"math"
)

var (
	op = flag.String("op", "", "Operation to perform")
	a  = flag.Float64("a", 0, "Operand A")
	b  = flag.Float64("b", 0, "Operand B")
)

type (
	Pow struct{}
	Sqr struct{}
	Abs struct{}
)

func main() {
	flag.Parse()

	var result float64
	var err error

	simpleCalc := calc.Simple()

	result, err = simpleCalc.Operation(*op, *a, *b)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(result)

	customCalc := calc.New()
	customCalc.AddOperation("pow", new(Pow))
	customCalc.AddOperation("sqr", new(Sqr))
	customCalc.AddOperation("abs", new(Abs))

	if result, err = customCalc.Operation("pow", 3, 4); err != nil {
		log.Fatalln(err)
	}
	fmt.Println(result)

	if result, err = customCalc.Operation("abs", -25); err != nil {
		log.Fatalln(err)
	}
	fmt.Println(result)

	if result, err = customCalc.Operation("sqr", 16); err != nil {
		log.Fatalln(err)
	}
	fmt.Println(result)
}

// Calculate – возведение в степень
func (Pow) Calculate(args ...float64) (result float64, err error) {
	if args[1] < 0 {
		err = fmt.Errorf("unable to raise a number to a negative power")
		return
	}

	result = math.Pow(args[0], args[1])
	return
}

// Calculate – квадратный корень
func (Sqr) Calculate(args ...float64) (result float64, err error) {
	if args[0] < 0 {
		err = fmt.Errorf("сannot find the square root of a negative number")
		return
	}

	result = math.Sqrt(args[0])
	return
}

// Calculate – модуль числа
func (Abs) Calculate(args ...float64) (result float64, err error) {
	result = math.Abs(args[0])
	return
}
