package calc

import (
	"fmt"
)


// MathOperation interface
type MathOperation interface {
	Calculate (args ...float64) (float64, error)
}

// Calculator struct
type Calculator struct {
	operations map[string]MathOperation
}

const (
	ADD = "add"
	SUB = "sub"
	MUL = "mul"
	DIV = "div"
)

type (
	Add struct{}
	Sub struct{}
	Mul struct{}
	Div struct{}
)


// New function
func New() *Calculator {
	c := new(Calculator)
	c.operations = make(map[string]MathOperation)
	return c
}

// Simple function
func Simple() *Calculator {
	c := New()
	c.AddOperation(ADD, new(Add))
	c.AddOperation(SUB, new(Sub))
	c.AddOperation(MUL, new(Mul))
	c.AddOperation(DIV, new(Div))
	return c
}


// AddOperation function
func (c *Calculator) AddOperation(op string, f MathOperation) {
	c.operations[op] = f
}


// Operation function
func (c *Calculator) Operation(op string, args ...float64) (result float64, err error) {
	defer func() {
		if recover() != nil {
			err = fmt.Errorf("not enought arguments for operation <%s>", op)
		}
	}()

	if operation, ok := c.operations[op]; ok {
		result, err = operation.Calculate(args...)
		return
	}

	err = fmt.Errorf("operation <%s> is not implemented", op)
	return
}


func (Add) Calculate(args ...float64) (result float64, err error) {
	result = args[0] + args[1]
	return
}


func (Sub) Calculate(args ...float64) (result float64, err error) {
	result = args[0] - args[1]
	return
}


func (Mul) Calculate(args ...float64) (result float64, err error) {
	result = args[0] * args[1]
	return
}


func (Div) Calculate(args ...float64) (result float64, err error) {
	if args[1] == 0 {
		err = fmt.Errorf("division by zero")
		return
	}

	result = args[0] / args[1]
	return
}
