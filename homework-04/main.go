package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const Separator = " " // Разделитель чисел в строке

// Поскольку из формулировки задания не очень понятно, как вводятся числа,
// сделал две функции ввода: из командной строки и из STDIN

func main() {
	var m []float64

	if len(os.Args) > 1 {
		m = readSliceFromArgs()
	} else {
		m = readSliceFromStdin(Separator)
	}

	if len(m) > 1 {
		// Тестировал функцию сортировки на скорость выполнения
		// в сравнении с функцией из пакета sort

		// sortStart := time.Now()
		// sort.Float64s(m)
		sortByInserts(m)
		// sortDone := time.Since(sortStart)
		// fmt.Println(sortDone)
	}

	fmt.Println(sliceToString(m, Separator))
}

// sortByInserts – функция сортировки вставками
func sortByInserts(m []float64) {
	for i := 0; i < len(m); i++ {
		j := i - 1
		value := m[i]
		for j >= 0 && m[j] > value {
			m[j+1] = m[j]
			j -= 1
		}
		m[j+1] = value
	}
}

// readSliceFromArgs – функция преобразует числа из командной строки,
// разделенные пробелом, в слайс значений float64.
// Значения, которые невозможно преобразовать в число, игнорируются.
func readSliceFromArgs() (result []float64) {
	for i := 1; i < len(os.Args); i++ {
		if value, err := strconv.ParseFloat(os.Args[i], 64); err == nil {
			result = append(result, value)
		}
	}

	return
}

// readSliceFromStdin - функция преобразует числа из STDIN, разделенные
// строкой <sep> или символом новой строки, в слайс значений float64.
// Значения, которые невозможно преобразовать в число, игнорируются.
func readSliceFromStdin(sep string) (result []float64) {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		tokens := strings.Split(scanner.Text(), sep)
		for _, token := range tokens {
			if value, err := strconv.ParseFloat(token, 64); err == nil {
				result = append(result, value)
			}
		}
	}

	return
}

// sliceToString - функция преобразует слайс чисел <m> в строку,
// с разделителем, указанным в переменнной <sep>
func sliceToString(m []float64, sep string) string {
	var sl []string

	for _, item := range m {
		sl = append(sl, fmt.Sprint(item))
	}

	return strings.Join(sl, sep)
}
