package main

import (
	"errors"
	"fmt"
	"math"
	"strconv"
)

func main() {
	fmt.Printf("Эта программа вычисляет площадь треугольника по трем сторонам.\n\n")

	var A, B, C float64

	if err := readTriangleSide("A", &A); err != nil {
		fmt.Println(err)
		return
	}

	if err := readTriangleSide("B", &B); err != nil {
		fmt.Println(err)
		return
	}

	if err := readTriangleSide("C", &C); err != nil {
		fmt.Println(err)
		return
	}

	halfPerimeter := (A + B + C) / 2
	triangleArea := math.Sqrt(halfPerimeter * (halfPerimeter - A) * (halfPerimeter - B) * (halfPerimeter - C))

	fmt.Printf("\nПлощадь треугольника = %v\n", triangleArea)
}

// readTriangleSide - функция вводит с клавиатуры длину стороны треугольника
func readTriangleSide(sideName string, sideLength *float64) error {
	fmt.Printf("Длина стороны %v > ", sideName)

	var sideStr string
	var err error

	if _, err = fmt.Scanln(&sideStr); err != nil {
		return errors.New("Ошибка функции ввода")
	}

	if *sideLength, err = strconv.ParseFloat(sideStr, 64); err != nil {
		return errors.New("Ошибка: введенное значение невозможно преобразовать в число")
	}

	if *sideLength <= 0 {
		return errors.New("Ошибка: число должно быть больше ноля")
	}

	return nil
}
