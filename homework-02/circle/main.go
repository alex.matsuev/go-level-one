package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	fmt.Print("Эта программа вычисляет диаметр и длину окружности по заданной площади круга.\n\n")

	fmt.Print("Введите площадь круга\t> ")

	var areaStr string
	_, err := fmt.Scanln(&areaStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	areaFloat, err := strconv.ParseFloat(areaStr, 64)
	if err != nil || areaFloat <= 0 {
		fmt.Println("Ошибка: площадь круга должна быть числом больше ноля")
		return
	}

	D := math.Sqrt(areaFloat/math.Pi) * 2
	L := math.Pi * D

	fmt.Printf("Диаметр окружности\t= %v\n", D)
	fmt.Printf("Длина окружности\t= %v\n", L)
}
