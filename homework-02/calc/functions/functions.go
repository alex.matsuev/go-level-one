package functions

import "math"

// CalcFuncion type
type CalcFunction func(a, b float64) float64
type CalcRepo map[string]CalcFunction

// Repo
var Repo = CalcRepo{
	"add": CalcAdd,
	"sub": CalcSub,
	"mul": CalcMul,
	"div": CalcDiv,
	"mod": math.Mod,
	"pow": math.Pow,
	"sqr": CalcSqr,
}

// Implemented message
var Implemented = `Implemented operations:
"add"	– addition (A + B)
"sub"	– subtraction (A - B)
"mul"	– multiplication (A * B)
"div"	– division (A / B)
"mod"	– reminder of (A / B)
"pow"	– exponentiation (A ** B)
"sqr"	– square root of A
`

// CalcAdd function
func CalcAdd(a, b float64) float64 {
	return a + b
}

// CalcSub function
func CalcSub(a, b float64) float64 {
	return a - b
}

// CalcMul function
func CalcMul(a, b float64) float64 {
	return a * b
}

// CalcDiv function
func CalcDiv(a, b float64) float64 {
	return a / b
}

// CalcSqr function
func CalcSqr(a, b float64) float64 {
	_ = b
	return math.Sqrt(a)
}
