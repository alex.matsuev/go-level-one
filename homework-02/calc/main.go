package main

import (
	"flag"
	"fmt"
	"go-level-one/homework-02/calc/functions"
)

var (
	a    = flag.Float64("a", 0, "Operand one")
	b    = flag.Float64("b", 0, "Operand two")
	op   = flag.String("op", "", "Operation")
	help = flag.Bool("help", false, "Help")
)

var helpMessage = `Simple calculator usage:

	calc --a XXX --b YYY --op OPERATION

Flags --a and --b is operands for arithmetic functions.
Flag --op is symbolic notation for arithmetic operation.

For example, command "calc --a 2 --b 3 --op add" returns 5.

`

func main() {
	flag.Parse()

	// If --help flag is true or --op flag is empty,
	// then show help message and exit
	if *help || *op == "" {
		fmt.Println(helpMessage + functions.Implemented)
		return
	}

	// Check if operation implemented, then do it
	// else show error message
	if f, ok := functions.Repo[*op]; ok {
		fmt.Println(f(*a, *b))
	} else {
		fmt.Printf("Error: operation \"%v\" not implemented\n", *op)
	}
}
