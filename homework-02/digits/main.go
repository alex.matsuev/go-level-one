package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Print("Эта программа показывает количество единиц, десятков и сотен в трехзначном числе\n\n")

	fmt.Print("Введите число из трех цифр > ")

	var numberStr string
	_, err := fmt.Scan(&numberStr)
	if err != nil {
		fmt.Println(err)
		return
	}

	if numberInt, err := strconv.Atoi(numberStr); err != nil {
		fmt.Println("Ошибка: невозможно преобразовать введенное значение в число")
		return
	} else if numberInt < 100 || numberInt > 999 {
		fmt.Println("Ошибка: число должно быть в диапазоне от 100 до 999")
		return
	}

	fmt.Printf("Сотни\t– %v\n", string(numberStr[0]))
	fmt.Printf("Десятки\t– %v\n", string(numberStr[1]))
	fmt.Printf("Единицы\t– %v\n", string(numberStr[2]))
}
