package functions

import (
	"fmt"
	"math"
	"sort"
)

// CalcOperation type
type CalcFunction func(a, b float64) float64

// CalcOperation struct
type CalcOperation struct {
	Operation    CalcFunction
	NumArguments uint8
	Description  string
	Template     string
}

// CalcRepo map of CalcOperation
type CalcRepo map[string]CalcOperation

// Repo
var Repo = CalcRepo{
	"add":  CalcOperation{OperationAdd, 2, "addition (A + B)", "%v + %v"},
	"sub":  CalcOperation{OperationSub, 2, "subtraction (A - B)", "%v - %v"},
	"mul":  CalcOperation{OperationMul, 2, "multiplication (A * B)", "%v * %v"},
	"div":  CalcOperation{OperationDiv, 2, "division (A / B)", "%v / %v"},
	"mod":  CalcOperation{math.Mod, 2, "reminder of (A / B)", "%v mod %v"},
	"pow":  CalcOperation{math.Pow, 2, "exponentiation (A ** B)", "%v ** %v"},
	"perc": CalcOperation{OperationPerc, 2, "B percents of A", "%[2]v%% of %[1]v"},
	"sqr":  CalcOperation{OperationSqr, 1, "square root of A", "Square root of %v"},
	"abs":  CalcOperation{OperationAbs, 1, "absolute value of A", "Absolute value of %v"},
	"log":  CalcOperation{OperationLog, 1, "decimal logarithm of A", "Decimal logarithm of %v"},
	"ln":   CalcOperation{OperationLn, 1, "natural logarithm of A", "Natural logarithm of %v"},
}

// Implemented function return descriptions of all functions in Repo
func Implemented() string {
	result := "Implemented operations:\n\n"

	// Sort map by operation keys
	keys := make([]string, 0, len(Repo))
	for key := range Repo {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	// Build ordered list of operation descriptions
	for _, key := range keys {
		result += fmt.Sprintf("%v\t– %v\n", key, Repo[key].Description)
	}

	return result
}

/*
	OPERATIONS FOR CALCULATOR
*/

// OperationAdd function
func OperationAdd(a, b float64) float64 {
	return a + b
}

// OperationSub function
func OperationSub(a, b float64) float64 {
	return a - b
}

// OperationMul function
func OperationMul(a, b float64) float64 {
	return a * b
}

// OperationDiv function
func OperationDiv(a, b float64) float64 {
	return a / b
}

// OperationPerc function
func OperationPerc(a, b float64) float64 {
	return a / 100 * b
}

// OperationSqr function
func OperationSqr(a, b float64) float64 {
	_ = b
	return math.Sqrt(a)
}

// OperationAbs function
func OperationAbs(a, b float64) float64 {
	_ = b
	return math.Abs(a)
}

// OperationLog function
func OperationLog(a, b float64) float64 {
	_ = b
	return math.Log10(a)
}

// OperationLn function
func OperationLn(a, b float64) float64 {
	_ = b
	return math.Log(a)
}
