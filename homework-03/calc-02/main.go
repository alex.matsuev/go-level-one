package main

import (
	"flag"
	"fmt"
	"go-level-one/homework-03/calc-02/functions"
	"os"
)

var (
	a    = flag.Float64("a", 0, "Operand one")
	b    = flag.Float64("b", 0, "Operand two")
	op   = flag.String("op", "", "Operation")
	help = flag.Bool("help", false, "Show help message")
)

const errorMessage = "operation \"%v\" not implemented"

func main() {
	flag.Parse()

	// If --help flag is true then show help message and exit
	if *help {
		fmt.Println(HelpMessage + functions.Implemented())
		return
	}

	var err error

	// If -op flag is empty, go to interactive mode
	if *op == "" {
		err = interactiveMode(a, b, op)
	} else {
		err = commandLineMode(a, b, op)
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// Run calc in interactive mode
func interactiveMode(a *float64, b *float64, op *string) (err error) {
	fmt.Print("Simple calculator interactive mode\n\n")

	fmt.Print("Set operation\t: ")
	if _, err = fmt.Scanln(op); err != nil {
		return
	}

	if f, ok := functions.Repo[*op]; ok {
		if err = scanOperand("A", a); err != nil {
			return
		}

		if f.NumArguments == 2 {
			if err = scanOperand("B", b); err != nil {
				return
			}
		}

		fmt.Printf("\n"+f.Template+" = %[3]v\n", *a, *b, f.Operation(*a, *b))
	} else {
		err = fmt.Errorf(errorMessage, *op)
	}
	return
}

// Run calc in command line mode
func commandLineMode(a *float64, b *float64, op *string) (err error) {
	if f, ok := functions.Repo[*op]; ok {
		fmt.Println(f.Operation(*a, *b))
	} else {
		err = fmt.Errorf(errorMessage, *op)
	}
	return
}

// scanOperand function
func scanOperand(name string, ptrOperand *float64) (err error) {
	fmt.Printf("Set operand %v\t: ", name)
	fmt.Scanln(ptrOperand)
	return
}
