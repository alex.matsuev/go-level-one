package main

// HelpMessage
const HelpMessage = `Simple calculator usage:

	calc -a XXX -b YYY -op OPERATION

Flags -a and -b is operands for arithmetic functions.
Flag -op is symbolic notation for arithmetic operation.

For example, command "calc -a 2 -b 3 -op add" returns 5.

If -op flag is not defined, program starts in interactive mode.

`
