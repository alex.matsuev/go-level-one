package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	fmt.Printf("Программа вычисляет ряд простых чисел от 2-х до N\n\n")

	fmt.Printf("Введите число N: ")
	var n uint
	if _, err := fmt.Scanln(&n); err != nil || n < 2 {
		fmt.Println("Ошибка: число должно быть целым, положительным, больше либо равным 2")
		// Обнаружил особенность использования fmt.Scan. Если не удается поместить введенное
		// значение в переменную указанного типа (например uint), функция прерывает обработку
		// а неиспользованный остаток строки остается в STDIN и после завершения программы
		// вызывается, как новая команда.
		//
		// Как вариант, при возникновении ошибки, прочитать из STDIN остаток строки.
		bufio.NewReader(os.Stdin).ReadLine()
		return
	}

	// Тестировал разные варианты реализации функции на время выполнения:
	execStart := time.Now() // запоминаю время запуска функции
	arrayOfSimpleNumbers := getSimpleNumbers(n)
	execTime := time.Since(execStart) // вычисляю время выполнения функции

	fmt.Println(arrayOfSimpleNumbers)
	fmt.Printf("\nExecution time: %v\n", execTime)
}

// getSimpleNumbers - Функция возвращает массив простых чисел от 2-х до N.
// Если параметр N меньше двух, будет возвращен пустой массив.
//
// Можно, конечно, порассуждать на тему является ли 1 простым числом,
// но определение в Википедии однозначно определяет, что ряд
// простых чисел начинается с 2-х.
func getSimpleNumbers(n uint) (simpleNumbers []uint) {
	switch {
	case n > 2:
		var i uint
	LOOP:
		for i = 3; i <= n; i += 2 {
			for _, elem := range simpleNumbers {
				if i%elem == 0 {
					continue LOOP
				}
			}
			simpleNumbers = append(simpleNumbers, i)
		}
		fallthrough

	case n == 2:
		simpleNumbers = append([]uint{2}, simpleNumbers...)
		return

	default:
		return
	}
}
