package main

import (
	"flag"
	"fmt"
	"go-level-one/homework-06/calc"
)

var (
	a  = flag.Float64("a", 0, "Operand one")
	b  = flag.Float64("b", 0, "Operand two")
	op = flag.String("op", "", "Operation")
)

func main() {
	flag.Parse()

	c := calc.New()

	result, err := c.Calculate(*op, *a, *b)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	fmt.Println(result)
}
