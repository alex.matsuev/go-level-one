package calc

import (
	"fmt"
	"math"
	"strings"
)

// Calculator struct
type Calculator struct{}

const (
	opAdd  = "add"
	opSub  = "sub"
	opMul  = "mul"
	opDiv  = "div"
	opMod  = "mod"
	opAbs  = "abs"
	opSqrt = "sqrt"
	opPow  = "pow"
	opPerc = "perc"
)

var (
	errorNotANumber     = fmt.Errorf("not a number")
	errorIsInfinity     = fmt.Errorf("is infinity")
	errorNotImplemented = fmt.Errorf("operation not implemented")
	errorNotEnoughtArgs = fmt.Errorf("not enought arguments for operation")
)

// New function
func New() *Calculator {
	return new(Calculator)
}

// Calculate function
func (c *Calculator) Calculate(operation string, args ...float64) (result float64, err error) {
	// Паника может возникнуть, если какой-то из функций не хватает входных аргументов
	defer func() {
		if r := recover(); r != nil {
			err = errorNotEnoughtArgs
		}
	}()

	switch strings.ToLower(operation) {
	case opAdd:
		result = c.Add(args...)
	case opSub:
		result = c.Sub(args...)
	case opMul:
		result = c.Mul(args...)
	case opDiv:
		result = c.Div(args...)
	case opMod:
		result = c.Mod(args...)
	case opAbs:
		result = c.Abs(args...)
	case opSqrt:
		result = c.Sqrt(args...)
	case opPow:
		result = c.Pow(args...)
	case opPerc:
		result = c.Perc(args...)
	default:
		err = errorNotImplemented
		return
	}

	err = c.checkMathErrors(result)
	return
}

// checkMathErrors function
func (c *Calculator) checkMathErrors(a float64) error {
	switch {
	case math.IsInf(a, 0):
		return errorIsInfinity
	case math.IsNaN(a):
		return errorNotANumber
	default:
		return nil
	}
}

// Add function
func (c *Calculator) Add(args ...float64) float64 {
	return args[0] + args[1]
}

// Sub function
func (c *Calculator) Sub(args ...float64) float64 {
	return args[0] - args[1]
}

// Mul function
func (c *Calculator) Mul(args ...float64) float64 {
	return args[0] * args[1]
}

// Div function
func (c *Calculator) Div(args ...float64) float64 {
	return args[0] / args[1]
}

// Mod function
func (c *Calculator) Mod(args ...float64) float64 {
	return math.Mod(args[0], args[1])
}

// Abs function
func (c *Calculator) Abs(args ...float64) float64 {
	return math.Abs(args[0])
}

// Sqrt function
func (c *Calculator) Sqrt(args ...float64) float64 {
	return math.Sqrt(args[0])
}

// Pow function
func (c *Calculator) Pow(args ...float64) float64 {
	return math.Pow(args[0], args[1])
}

// Perc function
func (c *Calculator) Perc(args ...float64) float64 {
	return args[0] / 100 * args[1]
}
