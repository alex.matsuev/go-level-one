package fibonacci

import (
	"fmt"
	"math/big"
)

// Минимальный номер числа в ряду Фибоначчи
const minNumber uint64 = 1

// Сообщения об ошибках
const errArgOutOfRange = "Argument is out of range"
const errResultOutOfRange = "Result is out of range"

// Map для хранения уже вычисленных чисел Фибоначчи типа uint64
var uintMap = make(map[uint64]uint64)

// Map для хранения уже вычисленных чисел Фибоначчи типа big.Int
var bigMap = make(map[uint64]*big.Int)

// Максимальный номер числа в ряду Фибоначчи, результат вычисления
// которого можно сохранить в переменной типа big.Int
// Изменить значение можно функцией <SetMaxBigNumber>
// По умолчанию устанавливается значение 1_000_000.
var maxBigNumber uint64 = 1_000_000

// GetUint - функция возвращает N-е число в раду Фибоначчи (типа uint64).
// В случае выхода результата за пределы типа uint64 вернет ошибку.
func GetUint(n uint64) (result uint64, err error) {
	if n < minNumber {
		err = fmt.Errorf("fibonacci.GetUint: %s", errArgOutOfRange)
		return
	}

	var ok bool
	if result, ok = uintMap[n]; ok {
		return
	}

	switch n {
	case 1:
		result = 0
	case 2:
		result = 1
	default:
		var fibN1, fibN2 uint64

		if fibN1, err = GetUint(n - 1); err != nil {
			return
		}

		if fibN2, err = GetUint(n - 2); err != nil {
			return
		}

		result = fibN1 + fibN2

		if result < fibN1 {
			// Если результат меньше, чем предыдущее число в ряду Фибоначчи
			// значит произошла ошибка переполнения типа uint64
			err = fmt.Errorf("fibonacci.GetUint: %s", errResultOutOfRange)
		} else {
			uintMap[n] = result
		}
	}

	return
}

// GetBig - функция возвращает N-е число в раду Фибоначчи (типа *big.Int).
// Значение аргумента <n> в диапазоне от 1 до maxBigNumber
func GetBig(n uint64) (result *big.Int, err error) {
	if n > maxBigNumber || n < minNumber {
		err = fmt.Errorf("fibonacci.GetBig: %s", errArgOutOfRange)
		return
	}

	var ok bool
	if result, ok = bigMap[n]; ok {
		return
	}

	result = new(big.Int)

	switch n {
	case 1:
		result.SetInt64(0)
	case 2:
		result.SetInt64(1)
	default:
		var fibN1, fibN2 *big.Int

		if fibN1, err = GetBig(n - 1); err != nil {
			return
		}

		if fibN2, err = GetBig(n - 2); err != nil {
			return
		}

		result.Add(fibN1, fibN2)
		bigMap[n] = result
	}

	return
}

//
// Вспомогательные функции
//

// SetMaxBigNumber - функция
func SetMaxBigNumber(n uint64) {
	if n < minNumber {
		n = minNumber
	}
	maxBigNumber = n
}

// InitUint - функция инициализирует первые N значений ряда Фибоначчи
// в map'e <uintMap>. Может быть полезна, если в программе
// несколько раз вызывается функция <GetUint>
func InitUint(n uint64) error {
	_, err := GetUint(n)
	return err
}

// InitBig - функция инициализирует первые N значений ряда Фибоначчи
// в map'e <bigMap>. Может быть полезна, если в программе
// несколько раз вызывается функция <GetBig>
func InitBig(n uint64) error {
	_, err := GetBig(n)
	return err
}
