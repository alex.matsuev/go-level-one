package main

import (
	"fmt"
	fib "go-level-one/homework-05/fibonacci"
	"log"
	"os"
	"strconv"
)

func main() {
	var n uint64
	var err error

	if len(os.Args) > 1 {
		if n, err = strconv.ParseUint(os.Args[1], 10, 64); err != nil {
			log.Fatalln(err)
		}
	} else {
		fmt.Print("Программа вычисляет N-е число в раду Фибоначчи\n\n")
		fmt.Print("Введите значение N: ")
		fmt.Scanln(&n)
	}

	if result, err := fib.GetUint(n); err != nil {
		log.Fatalln(err)
	} else {
		fmt.Println(result)
	}
}
